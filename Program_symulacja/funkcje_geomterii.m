clearvars
%GEOMETRIA
%Po kolei deklarujemy zmienne zwi?zane z k?tem wychylenia steru i
%wyd?u?eniem linki steruj?cej (pseudo-si?ownika)
%Deklaracja kat�w
delta_0=linspace(-25,25,101);
delta=delta_0*pi/180;
%Pocz?tkowa d?ugo?? pseudo-si?ownika
x0=0.25;
%Promie? na jakim zaczepiony jest pseudo-si?ownika - odleg?o?? zamocowania
%linki od osi obrotu elementu steru
R=0.15;
%Wyliczenie poszczeg�lnych d?ugo??i si?ownika
x=sqrt((x0-R*sin(delta)).^2+4*R*R*(sin(delta/2).^4))-x0;
%-(25*pi/180)
%A zatem w naszym przypadku mamy teraz geometrie odwrotn?, teraz musimy
%uzyska? geomtri?. Aby to zrobi? (w innym przypadku 1-D lookup table si?
%psuje) musimy odr�ci? nasze wektory
x_2=fliplr(x);
delta_2=fliplr(delta);

%DYNAMIKA

v_min=24; %minimalna pr?dko?? lotu 
v_max=84; %maksymalna pr?dko?? lotu 
a_2=2.53; %gradient wsp�?czynnika si?y no?nej dla usterzenia - u nas g?�wne
%?r�d?o si?y no?nej 
a_k=0.069; %wsp�?czynnik umo?liwiaj?cy wzi?cie pod uwag? wzrostu oporu, 
%wyliczony zgodnei z Corke 
k_1=1.75; %analogiczny wsp�?czynnik z Corke, do wyliczenia oporu 
%paramtery goemtryczne naszego steru oraz usterzenia
h=2400;
rho_powietrza=0.9669;
S_SH=1.35;
v=linspace(v_min,v_max,61);
delta_3=linspace(0,25,51);
delta_4=delta_3*pi/180;
limit_1=length(delta_3);
limit_2=length(v);
%tworzymy dwuwymiarow? tablic? z si?? no?n? i oporem zaleznym od wychylenia
%steru

%prealokacja
L = zeros(limit_1, limit_2);
D = zeros(limit_1, limit_2);

for i=1:limit_1
    for j=1:limit_2
        L(i,j)=0.5*rho_powietrza*a_2*S_SH*v(j)*v(j)*delta_4(i);
        D(i,j)=0.5*rho_powietrza*v(j)*v(j)*S_SH*k_1*a_k.*delta_4(i);
    end
end

%Q - si?a dzia?aj?ca na nasz pseudo-sterownik. r_2 i r_1 to cechy
%gemotryczne naszego modelu 
r_2=R;
r_1=0.07;

%prealokacja
Wsp1 = zeros(limit_1, limit_2);
Wsp2 = zeros(limit_1, limit_2);
Q = zeros(limit_1, limit_2);

for i=1:limit_1
    for j=1:limit_2
        Wsp1(i,j)=((r_1/r_2)*cos(delta_4(i))*cos(delta_4(i))*(L(i,j)+D(i,j)));
        Wsp2(i,j)=(cos(delta_4(i))*cos(delta_4(i))*(L(i,j)+D(i,j)));
        Wsp2(i,j)=Wsp2(i,j)-cos(delta_4(i))*((sin(delta_4(i))*(L(i,j)-D(i,j))+cos(delta_4(i))*(L(i,j)+D(i,j))));
        Q(i,j)=sqrt(Wsp1(i,j)^2+Wsp2(i,j)^2);
        Q(i,j)=(Q(i,j)*(x0 - R*sin(delta_4(i)))) / (sqrt((x0-R*sin(delta_4(i)))^2+4*R*R*((sin(delta_4(i)/2))^4)));
    end
end

%R�WNANIA SI?OWNIKA 

%Wyliczenie momentu od gwintu zgodnie z metod? z PKM II
%?rednica podzia?owa gwintu 
d_2=0.0088;
%Skok gwintu 
P=0.002;
%cechy gwintu 
gamma=atan(P/(pi*d_2));
%k?t tarcia
rho_prim = 5.718*pi/180;
%?rednica efektywna nakr?tki (arytmetyczna zewn?trznej i wewn?trznej ?rednicy)
D_nak=0.02;
d_nak=0.01;
D_n=(D_nak+d_nak)/2;
mi_n=0.3;
%Moment dzia?aj?cy na gwint 
G=Q*0.5*(d_2*tan(gamma+rho_prim)+D_n*mi_n);

%Nast?pnie przeprowadzimy redukcj? modelu, aby rozwa?a? moment dzia?aj?cy
%tylko na wale od silnika - zgodnie z metod? z PKM III
%Na start przyjmiemy kilka sta?ych, bez ktr�ych nie stworzymy wiarygodnego
%modelu
%Indukcyjno?? uk?adu
L_e=0.000031;
%Rezystancja uk?adu 
R_e=0.0821;
%Sta?a m�wi?ca nam jaki moment wygnereuje silnik dla pradu jednego ampera
K_e=0.0537;
%Zast?pczy moment bezw?adnos?i odpowiadaj?cy UPN - ponownie, ?aden
%producent nie zamie?ci? go nigdzie, wi?c dajemy 1
I_z=0.15;
%Ko?cowy moment bezw?adno?ci wyniesie:
I_e=I_z+(pi/64)*(D_nak-d_nak)^4;

b_e = 1e-4;

%PID
kp_kryt=72000;
okres=0.25;
kp=kp_kryt*0.6;
Ti=0.125;
Td=Ti/8;