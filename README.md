# SW_SUL

Simulation of the Cessna 172 rudder operation. The aim of the project was to create a simulation of the Cessna 172 rudder operation in the MATLAB&Simulink environment.

The project was carried out as part of the course "Aerospace Systems Summation". Project members:
- Karol Bresler
- Łukasz Grabowski 
- Aleksandra Pakuła

The project was carried out under the watchful eye of [Mateusz Sochacki, M.Sc](https://www.meil.pw.edu.pl/ZAiOL/Pracownicy2/Mateusz-Sochacki).

https://www.overleaf.com/2479181761djngyzgqfxww

Project documentation is available only in Polish. 

