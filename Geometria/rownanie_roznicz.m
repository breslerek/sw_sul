syms i(t) w(t)
%wszystkie sta?e a_i to poszczeg�lne sta?e z naszego przypadku, po ich
%odpowiednim dobraniu mozemy sprawdzi? czy nasze wyniki si? zgadzaj? z
%simulinkiem
V=12;
M=0.12;
a1=V/L_e;
a2=R_e/L_e;
a3=K_e/L_e;
a4=K_e/I_e;
a5=M/I_e;
%stworzenie macierzy do r�wnania macierzowego X'=AX+B, gdzie X=[i;w],
A = [-a2 -a3; a4 0];
B = [a1; -a5];
X = [i; w];
%tu nast?puje wa?ny moment - definicja warunk�w pocz?tkowych, zak??dam ?e
%chyba pocz?tkowo nic si? nie dzieje
C = X(0) == [0; 0];
odes = diff(X) == A*X + B
[iSol(t), wSol(t)] = dsolve(odes, C);
iSol(t) = simplify(iSol(t))
wSol(t) = simplify(wSol(t))
%wy?wietlenie wynik�w
clf
fplot(wSol,[0 25])
hold on
fplot(iSol,[0 25])
grid on
% axis([-20 20 -25 25])
legend('w','i','Location','best')